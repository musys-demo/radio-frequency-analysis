
## 频谱分析演示应用程序

### 运行环境

OS     `Ubuntu CentOS`
NodeJS `v10.24.1`
NPM    `v6.14.12`

### 本地环境的应用开发

- **安装应用**

```bash
npm install
```

- **开发环境下 启动应用**

```shell
npm run start
```

启动完成后打开浏览器访问

[http://localhost:7000](http://localhost:7000)

> 如果需要更改启动端口，可在 `.env` 文件中配置

- **live部署应用**

```shell
npm run build
```




## 开发说明
### 路由配置及规则
- 路由规则请参照[umi文档](https://umijs.org/zh/guide/router.html#%E7%BA%A6%E5%AE%9A%E5%BC%8F%E8%B7%AF%E7%94%B1)
- 动态路由请求数据格式为
```js

// id为5项目无menuParentId为根路由,无route项
// id为51项目为字路由页面，其route项作为路由地址
// 路由地址所使用的页面内容为pages文件夹下内容，文件名与路由名需要一一对应
// 如下路由其对应的pages文件内容为
// pages
// |
// |---chart
//      |
//      |---Echarts
[{
    id: '5',
    breadcrumbParentId: '1',
    name: '表格',
    icon: 'code-o',
  },
  {
    id: '51',
    breadcrumbParentId: '5',
    menuParentId: '5',
    name: 'ECharts',
    icon: 'line-chart',
    route: '/chart/ECharts',
  },
...]

```

> 请求到的路由会持久存储到本地storage,开发时请注意增量变化或者数据清除


store 'routeList' saved the menu items

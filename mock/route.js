import { Constant } from './_utils'
const { ApiPrefix } = Constant

// 开始
//   桌面
//   运行中的应用
//   文件系统
//   任务监控
// 服务管理
//   服务创建
//   应用服务
//   系统服务
// 资源管理
//   计算资源
//   网络资源
//   密匙管理
// 系统管理
//   计算集群
//   系统模块
// 系统设置
//   服务设置
//   页面设置
// 用户管理
//   用户

const database = [
  {
    id: '1',
    name: '开始',
  },
  {
    id: '11',
    // breadcrumbParentId: '1',
    menuParentId: '1',
    name: '桌面',
    icon: 'home',
    route: '/dashboard',
  },
  {
    id: '12',
    breadcrumbParentId: '11',
    menuParentId: '1',
    name: '频谱分析',
    icon: 'apartment',
    route: '/cluster',
  }
]

module.exports = {
  [`GET ${ApiPrefix}/routes`](req, res) {
    res.status(200).json(database)
  },
}

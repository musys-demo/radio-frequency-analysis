import React, { Fragment } from 'react'
import { Table, Tag, Badge } from 'antd'
import PropTypes from 'prop-types'
import styles from './statustable.less'

const columns = [
  {
    title: '节点状态',
    dataIndex: 'status',
    key: 'status',
    render: (text, row) => (
      <Tag color={text ? '#87d068' : '#f4c414'}>
        {text ? '成功连接' : '未连接'}
      </Tag>
    ),
  },
  {
    title: '',
    dataIndex: 'detail',
    key: 'detail',
  },
  {
    title: '数量',
    dataIndex: 'count',
    key: 'count',
    render: (text, row) => (
      <Tag color={row.status ? 'green' : 'gold'}>{text}</Tag>
    ),
  },
  {
    title: '',
    dataIndex: 'desc',
    key: 'desc',
    render: (text, row) => (
      <Fragment>
        <Badge color={row.status ? '#87d068' : '#f4c414'} />
        {text}
      </Fragment>
    ),
  },
  {
    title: '',
    dataIndex: 'status',
    key: 'status-key',
    render: (text, row) => <Badge color={row.status ? '#87d068' : '#f4c414'} />,
  },
]

function StatusTable({ data = [], ...rest }) {
  return (
    <div className={styles.statustable}>
      <Table rowKey="id" {...rest} dataSource={data} columns={columns} />
    </div>
  )
}

StatusTable.propTypes = {}

export default StatusTable

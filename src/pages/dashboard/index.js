import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'dva'
import { Page } from 'components'
import { Divider, Statistic, Card, Button, Row, Col } from 'antd'
import Chart from 'react-apexcharts'
// import ApexCharts from 'apexcharts';
import styles from './index.less'
import { getRFAnalysisData } from '@/services/api-cluster'

const ws = new WebSocket('ws://localhost:5000/websocket')

const pivot = (arr, start = 0, end = arr.length + 1) => {
  const swap = (list, a, b) => ([list[a], list[b]] = [list[b], list[a]])

  let pivot = arr[start],
    pointer = start

  for (let i = start; i < arr.length; i++) {
    if (arr[i][0] < pivot[0]) {
      pointer++
      swap(arr, pointer, i)
    }
  }
  swap(arr, start, pointer)

  return pointer
}

const quickSort = (arr, start = 0, end = arr.length) => {
  let pivotIndex = pivot(arr, start, end)

  if (start >= end) return arr
  quickSort(arr, start, pivotIndex)
  quickSort(arr, pivotIndex + 1, end)

  return arr
}

class Dashboard extends PureComponent {
  constructor(props) {
    super(props)
    this.destroy = false

    // ================== series data ==================
    this.state = {
      series: [
        {
          name: '频段占用',
          data: [],
        },
      ],
      options: {
        chart: {
          id: 'area-datetime',
          type: 'area',
          height: 350,
          zoom: {
            autoScaleYaxis: true,
          },
          foreColor: '#FAFAFA',
        },
        dataLabels: {
          enabled: false,
        },
        markers: {
          size: 0,
          style: 'hollow',
        },
        xaxis: {
          type: 'datetime',
          min: new Date('08 Jan 2020').getTime(),
          tickAmount: 8,
        },
        tooltip: {
          theme: 'dark',
          fillSeriesColor: false,
          marker: {
            show: false,
          },
          onDatasetHover: {
            highlightDataSeries: true,
          },
          x: {
            format: 'dd MMM yyyy',
          },
        },
        fill: {
          colors: ['#FFFF00', '#FF8000'],
          type: 'gradient',
          gradient: {
            shadeIntensity: 1,
            opacityFrom: 1,
            opacityTo: 0.3,
            stops: [0, 100],
          },
        },
        stroke: {
          width: 1,
          colors: '#FFFF00',
        },
      },
      selection: 'all',
      computingTime: 32,
      fileNumber: 17,
      occupancyPercent: 6.17,
      calFileSize: 22,
    }
  }

  componentDidMount() {
    // this.setClusterData();
  }

  componentWillUnmount() {
    this.destroy = true
  }

  /**
   *  get rf analyzed data
   */
  getAnalyzedData = sDate => {
    return getRFAnalysisData(sDate).then(res => {
      return res.data
    })
  }

  /**
   * =======================================================
   *                update data on click
   * =======================================================
   */
  updateData(timeline) {
    var topLevel = this

    this.setState({
      selection: timeline,
    })

    switch (timeline) {
      case '0801':
        ApexCharts.exec(
          'area-datetime',
          'zoomX',
          new Date('08 Jan 2020').getTime(),
          new Date('09 Jan 2020').getTime()
        )
        this.setState({ computingTime: '--' })
        this.setState({ fileNumber: 22 })
        this.setState({ occupancyPercent: '--' })
        this.setState({ calFileSize: 4.5 })
        ws.onopen = function() {
          console.log('connected')
          ws.send('0801')
        }

        break
      case '0901':
        ApexCharts.exec(
          'area-datetime',
          'zoomX',
          new Date('09 Jan 2020').getTime(),
          new Date('10 Jan 2020').getTime()
        )
        this.setState({ computingTime: '--' })
        this.setState({ fileNumber: 77 })
        this.setState({ occupancyPercent: '--' })
        this.setState({ calFileSize: 15.8 })
        ws.onopen = function() {
          console.log('connected')
          ws.send('0901')
        }
        break
      case '1001':
        ApexCharts.exec(
          'area-datetime',
          'zoomX',
          new Date('10 Jan 2020').getTime(),
          new Date('11 Jan 2020').getTime()
        )
        this.setState({ computingTime: '--' })
        this.setState({ fileNumber: 77 })
        this.setState({ occupancyPercent: '--' })
        this.setState({ calFileSize: 15.8 })
        ws.onopen = function() {
          console.log('connected')
          ws.send('1001')
        }
        break
      case '1101':
        ApexCharts.exec(
          'area-datetime',
          'zoomX',
          new Date('11 Jan 2020').getTime(),
          new Date('12 Jan 2020').getTime()
        )
        this.setState({ computingTime: '--' })
        this.setState({ fileNumber: 77 })
        this.setState({ occupancyPercent: '--' })
        this.setState({ calFileSize: 15.8 })
        ws.onopen = function() {
          console.log('connected')
          ws.send('1101')
        }
        break
      case '1201':
        ApexCharts.exec(
          'area-datetime',
          'zoomX',
          new Date('12 Jan 2020').getTime(),
          new Date('13 Jan 2020').getTime()
        )
        this.setState({ computingTime: '--' })
        this.setState({ fileNumber: 77 })
        this.setState({ occupancyPercent: '--' })
        this.setState({ calFileSize: 15.8 })
        ws.onopen = function() {
          console.log('connected')
          ws.send('1201')
        }
        break
      case '1301':
        ApexCharts.exec(
          'area-datetime',
          'zoomX',
          new Date('13 Jan 2020').getTime(),
          new Date('14 Jan 2020').getTime()
        )
        this.setState({ computingTime: '--' })
        this.setState({ fileNumber: 48 })
        this.setState({ occupancyPercent: '--' })
        this.setState({ calFileSize: 9.7 })
        ws.onopen = function() {
          console.log('connected')
          ws.send('1301')
        }
        break
      case 'all':
        ApexCharts.exec(
          'area-datetime',
          'zoomX',
          new Date('08 Jan 2020').getTime(),
          new Date('14 Jan 2020').getTime()
        )
        this.setState({ computingTime: '--' })
        this.setState({ fileNumber: 378 })
        this.setState({ occupancyPercent: '--' })
        this.setState({ calFileSize: 77.3 })
        ws.onopen = function() {
          console.log('connected')
          ws.send('all')
        }
        break
      default:
    }

    ws.onmessage = function(evt) {
      var ws_data = JSON.parse(evt.data)
      console.log('ws data', ws_data)
      var sorted_data = quickSort(ws_data)
      sorted_data.pop()
      console.log('sorted data', sorted_data)
      ApexCharts.exec('area-datetime', 'updateSeries', [
        {
          data: sorted_data,
        },
      ])
    }
    ws.onclose = function() {
      // websocket is closed.
      console.log('close')
    }
  }

  render() {
    const { dashboard } = this.props
    const { data } = this.state

    return (
      <Page className={styles.dashboard}>
        <div className="globeheader">
          <div className="title">频谱数据智能分析</div>
        </div>
        <Row gutter={16}>
          {/* ---------------------------------------  selection buttons -------------------------------------  */}
          <Col span={24} style={{ paddingBottom: '50px' }}>
            {/* =============================== Jan 10th ========================== */}
            <Button
              id="one_year"
              type="primary"
              onClick={() => this.updateData('1001')}
              className={this.state.selection === 'one_year' ? 'active' : ''}
            >
              1月10日分析报表
            </Button>
            <Divider type="vertical" />

            {/* =============================== Jan 11th ========================== */}
            <Button
              id="ytd"
              type="primary"
              onClick={() => this.updateData('1101')}
              className={this.state.selection === 'ytd' ? 'active' : ''}
            >
              1月11日分析报表
            </Button>
            <Divider type="vertical" />

            {/* =============================== Jan 12th ========================== */}
            <Button
              id="all"
              type="primary"
              onClick={() => this.updateData('1201')}
              className={this.state.selection === 'all' ? 'active' : ''}
            >
              1月12日分析报表
            </Button>

            {/* =============================== Jan All ========================== */}
            {/* <Button id="all" type="primary"
								onClick={() => this.updateData('all')}
								className={(this.state.selection === 'all' ? 'active' : '')}>
								1月8日-1月13日
    					</Button> */}
          </Col>

          {/* ============================== 分析的频段 ========================== */}
          <Col span={8}>
            <Card
              bordered={false}
              bodyStyle={{
                padding: 20,
                textAlign: 'center',
              }}
            >
              <Statistic title="分析的频段" value="100-1500" suffix="MHZ" />
            </Card>
          </Col>

          {/* ============================== 信号点数量 ========================== */}
          <Col span={8}>
            <Card
              bordered={false}
              bodyStyle={{
                padding: 20,
                textAlign: 'center',
              }}
            >
              <Statistic
                title="分析的信号点数量"
                value={1188010 * this.state.fileNumber}
                suffix="个"
              />
            </Card>
          </Col>

          {/* ============================== 频段占用度 ========================== */}
          <Col span={8}>
            <Card
              bordered={false}
              bodyStyle={{
                padding: 20,
                textAlign: 'center',
              }}
            >
              <Statistic
                title="频段占用度"
                value={this.state.occupancyPercent}
                suffix="%"
              />
            </Card>
          </Col>

          {/* ============================== 文件个数 ========================== */}
          <Col span={8}>
            <Card
              bordered={false}
              bodyStyle={{
                padding: 20,
                textAlign: 'center',
              }}
            >
              <Statistic
                title="频谱文件数量"
                value={this.state.fileNumber}
                suffix="个"
              />
            </Card>
          </Col>

          {/* ============================== 数据量 ========================== */}
          <Col span={8}>
            <Card
              bordered={false}
              bodyStyle={{
                padding: 20,
                textAlign: 'center',
              }}
            >
              <Statistic
                title="数据量"
                value={this.state.calFileSize}
                suffix="GB"
              />
            </Card>
          </Col>

          {/* ============================== 处理时间 ========================== */}
          <Col span={8}>
            <Card
              bordered={false}
              bodyStyle={{
                padding: 20,
                textAlign: 'center',
              }}
            >
              <Statistic
                title="处理时间"
                value={this.state.computingTime}
                suffix="秒"
              />
            </Card>
          </Col>

          {/* ===============================data chart========================== */}
          <Col span={24}>
            <Chart
              options={this.state.options}
              series={this.state.series}
              type="area"
              height={350}
            />
          </Col>
        </Row>
      </Page>
    )
  }
}

Dashboard.propTypes = {
  dashboard: PropTypes.object,
  loading: PropTypes.object,
}

export default Dashboard

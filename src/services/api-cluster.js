import axios from 'axios'

// create an axios instance
const request = axios.create({
  baseURL: 'http://106.12.171.96:7300/mock/6002857dbc31472f6dc1e9e9/alphasys', // api-product的base_url
  timeout: 200, // request timeout
})

export const getRFAnalysisData = sDate => {
  return request({
    url: '/getRF',
    method: 'post',
    data: { op: 'rf', date: sDate },
    loading: 'spin',
  })
}

/**
 * get cluster information
 */
export const getClusterInfo = () => {
  return request({
    url: '/cluster',
    method: 'get',
    loading: 'spin',
  })
}

/**
 *  get components information
 */
export const getComponentsInfo = () => {
  return request({
    url: '/getComponentsInfo',
    method: 'get',
    loading: 'spin',
  })
}

/**
 *  get public keys
 */
export const getSecretsInfo = () => {
  return request({
    url: '/getSecretsInfo',
    method: 'get',
    loading: 'spin',
  })
}

/**
 *  get pending tasks data
 */
export const getPendingTasksServices = () => {
  return request({
    url: '/getPendingTasks',
    method: 'get',
    loading: 'spin',
  })
}

/**
 *  get running tasks data
 */
export const getRunningTasksServices = () => {
  return request({
    url: '/getRunningTasks',
    method: 'get',
    loading: 'spin',
  })
}

/**
 *  get completed tasks data
 */
export const getCompletedTasksServices = () => {
  return request({
    url: '/getCompletedTasks',
    method: 'get',
    loading: 'spin',
  })
}

module.exports = {
  siteName: '频谱数据智能分析',
  copyright: '©2022 天羽蜂',
  logoPath: '/alphasys_a_logo_blue.png',
  apiPrefix: 'http://106.12.161.226:7300/mock/5eb5c5c2599d62415329f5fa/srtc',
  fixedHeader: true, // sticky primary layout header

  /* Layout configuration, specify which layout to use for route. */
  layouts: [
    {
      name: 'primary',
      include: [/.*/],
      exclude: [/(\/(en|zh))*\/login/],
    },
  ],

  /* I18n configuration, `languages` and `defaultLanguage` are required currently. */
  i18n: {
    /* Countrys flags: https://www.flaticon.com/packs/countrys-flags */
    languages: [],
    defaultLanguage: 'zh',
  },
}
